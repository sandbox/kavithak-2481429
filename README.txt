-- SUMMARY --

The field disabled module allows the user to disable a field from Field UI


-- REQUIREMENTS --

field module


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

* Once you have installed the module, if you go to Edit Fields Tab, 
you will see Disable checkbox under the Required option.
Check that and Click on save. The field will be disabled in the form.
